package com.Pages;

import com.Browser.Utility.MyUtility;

public class HomePage extends MyUtility {
	
	public void goToLoginPage() {
		LoginPage loginPage=new LoginPage();
		loginPage.doLogin();
	}
	
	public void goToNewUserPage() throws InterruptedException {
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.addNewUserRegister();
	}
	
	public void goToNewStudentPage() throws InterruptedException {
		
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.addNewStudentRegister();
	}
	
	public void goToSearchBox() {
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.searchBox();
	}
	
	
	
}
