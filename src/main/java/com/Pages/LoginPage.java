package com.Pages;

import org.openqa.selenium.By;

import com.Browser.Utility.MyUtility;

public class LoginPage extends MyUtility{
	private static final By Email_Locator = By.xpath("//input[@type='email']");
	private static final By Password_Locator = By.xpath("//input[@type='password']");
	private static final By LoginButton_Locator = By.xpath("//button[@type='submit']");

	public void doLogin() {
		enterText(Email_Locator, "admin@yopmail.com");
		enterText(Password_Locator, "admin123");
		clickOn(LoginButton_Locator);
		
	}



}
