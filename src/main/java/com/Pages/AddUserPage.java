package com.Pages;

import org.openqa.selenium.By;

import com.Browser.Utility.MyUtility;

public class AddUserPage extends MyUtility{
	private static final By AddNewUser_Locator = By.xpath("//a[@routerlink='/add']");
	
	private static final By AddNewStudent_Locator=By.xpath("//button[@routerlink='/add']");
	
	private static final By FirstName_Locator = By.xpath("//input[@formcontrolname='first_name']");
	private static final By LastName_Locator = By.xpath("//input[@formcontrolname='last_name']");
	private static final By EmailBox_Locator = By.xpath("//input[@formcontrolname='email']");
	private static final By PhoneNumber_Locator = By.xpath("//input[@formcontrolname='phone']");
	private static final By RegisterButton_Locator = By.xpath("//button[@type='submit']");
	
	private static final By Search_Locator=By.xpath("//input[@type='text']");
	private static final String DeleteXpath = "//td[text()='TextToReplace ']/..//button[text()='Delete']";
	private static final By LogOut_Locator=By.xpath("//a[text()=' Logout']");
	
	public void addNewUserRegister() throws InterruptedException {
		Thread.sleep(3000);
		clickOn(AddNewUser_Locator);
		Thread.sleep(3000);
		enterText(FirstName_Locator, "John");
		enterText(LastName_Locator, "Angelo");
		enterText(EmailBox_Locator, "johnangelo@gmail.com");
		enterText(PhoneNumber_Locator, "8567568688");
		clickOn(RegisterButton_Locator);
		
	}
	
	public void addNewStudentRegister() throws InterruptedException {
		Thread.sleep(3000);
		clickOn(AddNewStudent_Locator);
		Thread.sleep(3000);
		enterText(FirstName_Locator, "Michael");
		enterText(LastName_Locator, "EXE");
		enterText(EmailBox_Locator, "Michaelex@gmail.com");
		enterText(PhoneNumber_Locator, "8569068688");
		clickOn(RegisterButton_Locator);
	} 
	
	public void searchBox() {
		enterText(Search_Locator, "Oman");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteUser(String userName) {
		String locator = DeleteXpath.replace("TextToReplace", userName);
		By Delete_Locator=By.xpath(locator);
		clickOn(Delete_Locator);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		acceptAlert();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void logOut() {
		clickOn(LogOut_Locator);
	}
	
}
