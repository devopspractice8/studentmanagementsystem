package com.Browser.Utility;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyUtility myBrowser=new MyUtility();
		myBrowser.launchLocalBrowser("Chrome");
		myBrowser.goTo("https://www.google.com");
		myBrowser.maximizeBrowser();
		myBrowser.setResolution(Device.IPAD);
		myBrowser.goTo("https://www.facebook.com");
		myBrowser.closeBrowserSession();
	}

}
