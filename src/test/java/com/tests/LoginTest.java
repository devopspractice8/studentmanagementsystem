package com.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Pages.HomePage;
import com.Pages.LoginPage;

public class LoginTest {
     private HomePage homePage;
	
	@BeforeMethod
	public void setUp() {
		homePage=new HomePage();
		homePage.launchLocalBrowser("Chrome");
		homePage.goTo("http://68.183.9.82/");
		homePage.maximizeBrowser();
	}

	@Test(testName = "Home Page",description = "Test for Home Page")
	public void loginPageTest() throws InterruptedException {
		LoginPage loginPage=new LoginPage();
		Thread.sleep(2000);
		loginPage.doLogin();

	}
	
	@AfterMethod
	public void delete() {
		try {
			homePage.closeBrowserSession();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}