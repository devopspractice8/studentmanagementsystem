package com.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Pages.AddUserPage;
import com.Pages.HomePage;

public class AddStudentTest {
private HomePage homePage;
	
	@BeforeMethod
	public void setUp() throws InterruptedException {
		homePage=new HomePage();
		homePage.launchLocalBrowser("Chrome");
		homePage.goTo("http://68.183.9.82/");
		homePage.maximizeBrowser();
		homePage.goToLoginPage();
		Thread.sleep(2000);
	}
	
	@Test(testName="Add New Student",description = "Test for Add a New Student")
	public void addStudentTest() throws InterruptedException {
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.addNewStudentRegister();
		
	}
	
	@AfterMethod
	public void delete() {
		try {
			AddUserPage addUserPage=new AddUserPage();
			addUserPage.deleteUser("Michael");
		} catch (Exception e) {
			// TODO: handle exception
		}
		homePage.closeBrowserSession();
	}
}
